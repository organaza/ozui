import { ModalContainerDirective } from './modal-container.directive';
import { TestBed, async } from '@angular/core/testing';

describe('ModalContainerDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalContainerDirective ]
    })
    .compileComponents();
  }));
});

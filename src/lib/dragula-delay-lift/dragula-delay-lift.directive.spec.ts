import { DragulaDelayLiftDirective } from './dragula-delay-lift.directive';
import { TestBed, async } from '@angular/core/testing';

describe('DragulaDelayLiftDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragulaDelayLiftDirective ],
    })
    .compileComponents();
  }));
});

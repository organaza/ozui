import { TooltipDirective } from './tooltip.directive';
import { TestBed, async } from '@angular/core/testing';

describe('TooltipDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TooltipDirective ]
    })
    .compileComponents();
  }));
});

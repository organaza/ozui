import { SortTableColumnDirective } from './sorttable-column.directive';
import { TestBed, async } from '@angular/core/testing';

describe('SortTableColumnDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortTableColumnDirective ]
    })
    .compileComponents();
  }));
});

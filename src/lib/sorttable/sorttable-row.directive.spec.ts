import { SortTableRowDirective } from './sorttable-row.directive';
import { TestBed, async } from '@angular/core/testing';

describe('SortTableRowDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortTableRowDirective ]
    })
    .compileComponents();
  }));
});

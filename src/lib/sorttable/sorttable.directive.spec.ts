import { SortTableDirective } from './sorttable.directive';
import { TestBed, async } from '@angular/core/testing';

describe('SortTableDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortTableDirective ]
    })
    .compileComponents();
  }));
});

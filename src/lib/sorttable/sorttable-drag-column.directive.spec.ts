import { SortTableDragColumnDirective } from './sorttable-drag-column.directive';
import { TestBed, async } from '@angular/core/testing';

describe('SortTableDragColumnDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortTableDragColumnDirective ]
    })
    .compileComponents();
  }));
});

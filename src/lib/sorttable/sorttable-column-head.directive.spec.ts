import { SortTableColumnHeadDirective } from './sorttable-column-head.directive';
import { TestBed, async } from '@angular/core/testing';

describe('SortTableColumnHeadDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortTableColumnHeadDirective ]
    })
    .compileComponents();
  }));
});

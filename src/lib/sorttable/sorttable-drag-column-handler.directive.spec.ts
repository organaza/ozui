import { SortTableDragColumnHandlerDirective } from './sorttable-drag-column-handler.directive';
import { TestBed, async } from '@angular/core/testing';

describe('SortTableDragColumnHandlerDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortTableDragColumnHandlerDirective ]
    })
    .compileComponents();
  }));
});

import { SortTableRowHandlerDirective } from './sorttable-row-handler.directive';
import { TestBed, async } from '@angular/core/testing';

describe('SortTableRowHandlerDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortTableRowHandlerDirective ]
    })
    .compileComponents();
  }));
});

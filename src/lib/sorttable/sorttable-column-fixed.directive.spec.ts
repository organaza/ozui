import { SortTableColumnFixedDirective } from './sorttable-column-fixed.directive';
import { TestBed, async } from '@angular/core/testing';

describe('SortTableColumnFixedDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortTableColumnFixedDirective ]
    })
    .compileComponents();
  }));
});

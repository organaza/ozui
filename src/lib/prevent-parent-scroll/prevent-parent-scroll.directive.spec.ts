import { PreventParentScrollDirective } from './prevent-parent-scroll.directive';
import { TestBed, async } from '@angular/core/testing';

describe('PreventParentScrollDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreventParentScrollDirective ]
    })
    .compileComponents();
  }));
});
